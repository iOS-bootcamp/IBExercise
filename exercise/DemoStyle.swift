//
//  DemoStyle.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit


class DemoStyle {
    enum FontColour {
        case grey
        case darkGrey
        case red
        case blue
        case violet
        
        func name() -> String {
            switch self {
            case .grey:
                return "grey"
            case .darkGrey:
                return "darkGrey"
            case .blue:
                return "blue"
            case .red:
                return "red"
            case .violet:
                return "violet"
            }
        }
    }
    
    class func makeUIColourFromString(withFontString fontString: String) -> UIColor {
        switch fontString {
        case FontColour.grey.name():
            return UIColor.gray
        case FontColour.darkGrey.name():
            return UIColor.darkGray
        case FontColour.red.name():
            return UIColor(colorLiteralRed: 255, green: 0, blue: 0, alpha: 1)
        case FontColour.blue.name():
            return UIColor.blue
        case FontColour.violet.name():
            return UIColor(rgb: 0x8A2BE2)
        default:
            return UIColor.gray
        }
    }
}
