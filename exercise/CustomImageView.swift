//
//  CustomImageView.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomImageView: UIImageView {
    
    override func prepareForInterfaceBuilder() {
        configure()
    }
    
    @IBInspectable var tintColourText: String = "grey" {
        didSet {
            let resolvedFontColour: UIColor = DemoStyle.makeUIColourFromString(withFontString: tintColourText)
            tintColor = resolvedFontColour
        }
    }
    
    private func configure() {
        self.image = self.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
    }
}
