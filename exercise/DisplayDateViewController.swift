//
//  DatePickerDisplayViewController.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit

class DisplayDateViewController: UIViewController {
    let displayInputSegue: String = "displayInputSegue"
    var currentDate: Date?
    
    @IBOutlet weak var dateField: CustomTextField!
    
    @IBAction func onDateSelected(_ sender: Any) {
        self.performSegue(withIdentifier: displayInputSegue, sender: self)
    }
    
    @IBAction func unwindToDateDisplay(segue: UIStoryboardSegue) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let date = currentDate {
            dateField.text = date.description
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == displayInputSegue {
            
        }
    }
}
