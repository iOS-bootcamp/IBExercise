//
//  AssetViewController.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class AssetViewCompleteViewController: UIViewController {
    @IBOutlet weak var currentDate: UITextField!
    @IBOutlet weak var calendarIcon: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        calendarIcon.tintColor = UIColor.brown
    }
}

