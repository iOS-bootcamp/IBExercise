//
//  CustomTextField.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CustomTextField: UITextField {
    @IBInspectable var fontColourText: String = "grey" {
        didSet {
            let resolvedFontColour: UIColor = DemoStyle.makeUIColourFromString(withFontString: fontColourText)
            textColor = resolvedFontColour
        }
    }
}

