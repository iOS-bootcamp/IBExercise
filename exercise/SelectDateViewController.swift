//
//  SelectDateViewController.swift
//  exercise
//
//  Created by Wade Reweti on 26/04/17.
//  Copyright © 2017 NativeMobile. All rights reserved.
//

import Foundation
import UIKit

class SelectDateViewController: UIViewController {
    let unwindToDisplay: String = "unwindToDisplay"
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    @IBAction func onDoneSelected(_ sender: Any) {
        self.performSegue(withIdentifier: unwindToDisplay, sender: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == unwindToDisplay {
            let currentDate:Date = datePicker.date
            
            (segue.destination as! DisplayDateViewController).currentDate = currentDate
        }
    }
}
